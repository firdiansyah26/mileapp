// 6.a
function changeVar() {
    var a = 3, b = 5
    console.log('NILAI AWAL')
    console.log('a : ', a)
    console.log('b : ', b)

    a = a + b
    b = a - b
    a = a - b

    console.log('NILAI AKHIR')
    console.log('a : ', a)
    console.log('b : ', b)
}

changeVar()

// 6.b
function findMissingNumber() {
    var numbers = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100];
    var missingNumber = []
    for (var index = 1; index < numbers.length; index++) {
        if (numbers[index] - numbers[index - 1] != 1) {
            missingNumber.push(numbers[index] - 1)
        }
    }

    console.log('Missing number : ', missingNumber)
}

findMissingNumber()

// 6.c
function returnNumber() {
    var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];
    var lastNumber = 0, collectNumber = []

    for (let index = 0; index < numbers.length; index++) {
        if (lastNumber == numbers[index]) {
            if (collectNumber.filter(obj => obj == numbers[index]).length == 0) {
                collectNumber.push(numbers[index])
            }
        }
        lastNumber = numbers[index]
    }

    console.log('collectNumber : ', collectNumber)
}

returnNumber()

// 6.d
var array_code = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3."]

let addObject = (obj_, arr_, newData) => {
    const obj = typeof obj_ === 'string' ? {} : obj_
    if (arr_.length === 0) return newData
    const [head, ...tail] = arr_
    return {
        ...obj,
        [head]: addObject(obj[head] || {}, tail, newData),
    }
}

function mappingObj() {
    let result = array_code.reduce(
        (obj, path) => addObject(obj, path.split('.').slice(0, -1), path),
        {}
    )

    console.log('result : ', result)
}

mappingObj()